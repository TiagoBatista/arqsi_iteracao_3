﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Lugares.Models;
using ModelLibrary.IdentityModels;
using ModelLibrary;

namespace Lugares.Controllers
{
    public class LocaisController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Locais
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> Index()
        {
            return View(await db.Locals.ToListAsync());
        }

        // GET: Locais/Details/5
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Local local = await db.Locals.FindAsync(id);
            if (local == null)
            {
                return HttpNotFound();
            }
            return View(local);
        }

        // GET: Locais/Create
        [Authorize(Roles = "Editor")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Locais/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> Create([Bind(Include = "LocalId,GPS_Lat,GPS_Long,nome")] Local local)
        {
            if (ModelState.IsValid)
            {
                db.Locals.Add(local);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(local);
        }

        // GET: Locais/Edit/5
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Local local = await db.Locals.FindAsync(id);
            if (local == null)
            {
                return HttpNotFound();
            }
            return View(local);
        }

        // POST: Locais/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> Edit([Bind(Include = "LocalId,GPS_Lat,GPS_Long,nome")] Local local)
        {
            if (ModelState.IsValid)
            {
                db.Entry(local).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(local);
        }

        // GET: Locais/Delete/5
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Local local = await db.Locals.FindAsync(id);
            if (local == null)
            {
                return HttpNotFound();
            }
            return View(local);
        }

        // POST: Locais/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Local local = await db.Locals.FindAsync(id);
            db.Locals.Remove(local);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Editor")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
