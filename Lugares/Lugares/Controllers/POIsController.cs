﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Lugares.Models;
using ModelLibrary.IdentityModels;
using Microsoft.AspNet.Identity;
using ModelLibrary;
using ModelLibrary.Validations;

namespace Lugares.Controllers
{
    public class POIsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: POIs
        [Authorize(Roles ="Editor")]
        public async Task<ActionResult> Index()
        {
            var pOIs = db.POIs.Include(p => p.local);
            return View(await pOIs.ToListAsync());
        }

        // GET: POIs/Details/5
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = await db.POIs.FindAsync(id);
            if (pOI == null)
            {
                return HttpNotFound();
            }
            return View(pOI);
        }

        // GET: POIs/Create
        [Authorize(Roles = "Editor")]
        public ActionResult Create()
        {
            ViewBag.LocalId = new SelectList(db.Locals, "LocalId", "nome");
            return View();
        }

        // POST: POIs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Editor")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "POIId,nome,Descricao,LocalId")] POI pOI)
        {
            if (ModelState.IsValid)
            {
                pOI.userID = User.Identity.GetUserId();
                db.POIs.Add(pOI);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.LocalId = new SelectList(db.Locals, "LocalId", "nome", pOI.LocalId);
            return View(pOI);
        }

        // GET: POIs/Edit/5
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = await db.POIs.FindAsync(id);
            if (pOI.userID!=User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (pOI == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalId = new SelectList(db.Locals, "LocalId", "nome", pOI.LocalId);
            return View(pOI);
        }

        // POST: POIs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Editor")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "POIId,nome,Descricao,LocalId")] POI pOI)
        {
            
            if (Val.userIsCreator(User,pOI))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (ModelState.IsValid)
            {
                db.Entry(pOI).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.LocalId = new SelectList(db.Locals, "LocalId", "nome", pOI.LocalId);
            return View(pOI);
        }

        // GET: POIs/Delete/5
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> Delete(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            POI pOI = await db.POIs.FindAsync(id);
            if (pOI.userID != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (pOI == null)
            {
                return HttpNotFound();
            }
            return View(pOI);
        }

        // POST: POIs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Editor")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            POI pOI = await db.POIs.FindAsync(id);
            if (Val.userIsCreator(User, pOI))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            db.POIs.Remove(pOI);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "Editor")]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       
    }

    
}
