namespace Lugares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class upd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Locals",
                c => new
                    {
                        LocalId = c.Int(nullable: false, identity: true),
                        GPS_Lat = c.Single(nullable: false),
                        GPS_Long = c.Single(nullable: false),
                        nome = c.String(),
                    })
                .PrimaryKey(t => t.LocalId);
            
            AddColumn("dbo.POIs", "userID", c => c.String());
            CreateIndex("dbo.POIs", "LocalId");
            AddForeignKey("dbo.POIs", "LocalId", "dbo.Locals", "LocalId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.POIs", "LocalId", "dbo.Locals");
            DropIndex("dbo.POIs", new[] { "LocalId" });
            DropColumn("dbo.POIs", "userID");
            DropTable("dbo.Locals");
        }
    }
}
