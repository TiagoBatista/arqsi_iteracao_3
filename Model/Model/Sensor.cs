﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ModelLibrary.Sensors
{
    public class Sensor
    {
        public int SensorId { get; set;}
        public String nome { get; set;}
        public String descricao { get; set; }
    }
    public class Facetas
    {
        [Key]
        public int FacetasID { get; set; }

        public String CampoBD { get; set; }
        public String Nome { get; set; }
        public String Grandeza { get; set; }
        public String Tipo { get; set; }
        public String Semantica { get; set; }
        
        public int sensorId { get; set; }

    }
}