﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary
{
    public class POI
    {
        public int POIId { get; set; }
        [DisplayName("Point of Interest")]
        public String nome { get; set; }
        public String Descricao { get; set; }
        [DisplayName("Local")]
        public int LocalId { get; set; }
        public virtual Local local { get; set; }
        public virtual string userID { get; set; }

    }

    public class Local
    {
        public int LocalId { get; set; }
        public float GPS_Lat { get; set; }
        public float GPS_Long { get; set; }
        [DisplayName("Local")]
        public String nome { get; set; }
    }
    
    public class Metereologia
    {
        public int MetereologiaId {get;set;}
        public DateTime Data_de_leitura { get; set; }
        public TimeSpan Hora_de_leitura { get; set; }
        public float Temp { get; set; }
        public float Vento { get; set; }
        public float Humidade { get; set; }
        public float Pressao { get; set;}
        public float NO { get; set; }
        public float NO2 { get; set; }
        public float CO2 { get; set; }
        public int LocalId { get; set; }
        public virtual Local local { get; set; }
    }
}
