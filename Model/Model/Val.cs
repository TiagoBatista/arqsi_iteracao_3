﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLibrary.IdentityModels;
using Microsoft.AspNet.Identity;
using System.Security.Principal;

namespace ModelLibrary.Validations
{
    public static class Val
    {
        public static bool userIsCreator(IPrincipal user,POI pOI)
        {
            if (user.Identity.GetUserId() == pOI.userID)
            {
                return true;
            }else
            {
                return false;
            }
        }
    }
}
