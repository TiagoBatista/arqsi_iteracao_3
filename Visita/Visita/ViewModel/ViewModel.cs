﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Visita.ViewModel
{
    public class viewModel
    {
        [DataType(DataType.Date)]
        [DisplayName("Data de Leitura Inicial")]
        public DateTime data_de_leitura_inicio { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Data de Leitura Final")]
        public DateTime data_de_leitura_fim { get; set; }

        [DataType(DataType.Time)]
        [DisplayName("Hora de Leitura Inicial")]
        public TimeSpan hora_de_leitura_inicio { get; set; }

        [DataType(DataType.Time)]
        [DisplayName("Hora de Leitura Final")]
        public TimeSpan hora_de_leitura_fim { get; set; }
    }
}