﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Visita.Helpers;
using ModelLibrary;
using Visita.ViewModel;

namespace Visita.Controllers
{
    public class ApiRequestsController : Controller
    {
        // GET: ApiRequests
        public async System.Threading.Tasks.Task<ActionResult> Index() {
            
                return View();
            
        }
        //GET: ApiRequests/SearchByPOI
        public async System.Threading.Tasks.Task<ActionResult> SearchByPOI()
        {
            
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/POIs");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var pois =
                JsonConvert.DeserializeObject<IEnumerable<POI>>(content);
                ViewBag.POIId = new SelectList(pois, "POIId", "nome");
                return View();
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        public async System.Threading.Tasks.Task<ActionResult> SearchByPOItoAPI(POI p)
        {
            int id = p.POIId;
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Requests/"+id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var mete =
                JsonConvert.DeserializeObject<IEnumerable<Metereologia>>(content);
                return View(mete);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

        }

        public async System.Threading.Tasks.Task<ActionResult> SearchDatenHour()
        {
            return View();
        }

        public async System.Threading.Tasks.Task<ActionResult> SearchDatenHourtoAPI(viewModel vm) 
        {
            var client = WebApiHttpClient.GetClient();
            DateTime dt = vm.data_de_leitura_inicio;
            TimeSpan ts = vm.hora_de_leitura_inicio;

            String data = dt.Year + "-" + dt.Month + "-" + dt.Day;
            String hora = ts.Hours+":"+ts.Minutes+":"+ts.Seconds;

            HttpResponseMessage response = await client.GetAsync("api/Requests?dt="+data+""+"&ts="+hora);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var mete =
                JsonConvert.DeserializeObject<IEnumerable<Metereologia>>(content);
                return View(mete);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

        }

        public async System.Threading.Tasks.Task<ActionResult> SearchDatenHourRange()
        {

            return View();

        }

        public async System.Threading.Tasks.Task<ActionResult> SearchDatenHourRangetoAPI(viewModel vm)
        {
            
            var client = WebApiHttpClient.GetClient();
            DateTime dt1 = vm.data_de_leitura_inicio;
            TimeSpan ts1 = vm.hora_de_leitura_inicio;

            String data1 = dt1.Year + "-" + dt1.Month + "-" + dt1.Day;
            String hora1 = ts1.Hours + ":" + ts1.Minutes + ":" + ts1.Seconds;

            DateTime dt2 = vm.data_de_leitura_fim;
            TimeSpan ts2 = vm.hora_de_leitura_fim;

            String data2 = dt2.Year + "-" + dt2.Month + "-" + dt2.Day;
            String hora2 = ts2.Hours + ":" + ts2.Minutes + ":" + ts2.Seconds;

            HttpResponseMessage response = await client.GetAsync("api/Requests?dt1="+data1+"&dt2="+data2+"&ts1="+hora1+"&ts2="+hora2);

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var mete =
                JsonConvert.DeserializeObject<IEnumerable<Metereologia>>(content);
                return View(mete);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

        }

    }
}