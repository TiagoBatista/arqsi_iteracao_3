namespace Cancela.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;
    using Microsoft.AspNet.Identity;
    using ModelLibrary.IdentityModels;

    internal sealed class Configuration : DbMigrationsConfiguration<ModelLibrary.IdentityModels.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ModelLibrary.IdentityModels.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            const string roleName = "Admin";
            const string userName = "admin@admin.pt";
            const string password = "password";

            //verificar se role existe antes de o adicionar
            if (!context.Roles.Any(r => r.Name == roleName))
            {
                context.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole(roleName));
                context.SaveChanges();
            }

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            ApplicationUser user = null;

            //antes de adicionar o user verificar se este existe
            if (!context.Users.Any(u => u.UserName == userName))
            {
                user = new ModelLibrary.IdentityModels.ApplicationUser
                {
                    UserName = userName
                };
                userManager.Create(user, password);
                context.SaveChanges();
            }
            else {//utilizador ja existe
                user = context.Users.Single(u => u.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase));
            }
            userManager.AddToRole(user.Id, roleName);
            context.SaveChanges();
        }
    }
}
