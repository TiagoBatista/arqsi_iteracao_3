namespace Cancela.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class t : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Metereologias",
                c => new
                    {
                        MetereologiaId = c.Int(nullable: false, identity: true),
                        data_de_leitura = c.DateTime(nullable: false),
                        hora_de_leitura = c.DateTime(nullable: false),
                        temp = c.Single(nullable: false),
                        vento = c.Single(nullable: false),
                        humidade = c.Single(nullable: false),
                        pressao = c.Single(nullable: false),
                        NO = c.Single(nullable: false),
                        NO2 = c.Single(nullable: false),
                        CO2 = c.Single(nullable: false),
                        LocalId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MetereologiaId)
                .ForeignKey("dbo.Locals", t => t.LocalId, cascadeDelete: true)
                .Index(t => t.LocalId);
            
            //CreateTable(
            //    "dbo.Locals",
            //    c => new
            //        {
            //            LocalId = c.Int(nullable: false, identity: true),
            //            GPS_Lat = c.Single(nullable: false),
            //            GPS_Long = c.Single(nullable: false),
            //            nome = c.String(),
            //        })
            //    .PrimaryKey(t => t.LocalId);
            
            //CreateTable(
            //    "dbo.POIs",
            //    c => new
            //        {
            //            POIId = c.Int(nullable: false, identity: true),
            //            nome = c.String(),
            //            Descricao = c.String(),
            //            LocalId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.POIId)
            //    .ForeignKey("dbo.Locals", t => t.LocalId, cascadeDelete: true)
            //    .Index(t => t.LocalId);
            
        //    CreateTable(
        //        "dbo.AspNetRoles",
        //        c => new
        //            {
        //                Id = c.String(nullable: false, maxLength: 128),
        //                Name = c.String(nullable: false, maxLength: 256),
        //            })
        //        .PrimaryKey(t => t.Id)
        //        .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        //    CreateTable(
        //        "dbo.AspNetUserRoles",
        //        c => new
        //            {
        //                UserId = c.String(nullable: false, maxLength: 128),
        //                RoleId = c.String(nullable: false, maxLength: 128),
        //            })
        //        .PrimaryKey(t => new { t.UserId, t.RoleId })
        //        .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
        //        .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
        //        .Index(t => t.UserId)
        //        .Index(t => t.RoleId);
            
        //    CreateTable(
        //        "dbo.AspNetUsers",
        //        c => new
        //            {
        //                Id = c.String(nullable: false, maxLength: 128),
        //                Email = c.String(maxLength: 256),
        //                EmailConfirmed = c.Boolean(nullable: false),
        //                PasswordHash = c.String(),
        //                SecurityStamp = c.String(),
        //                PhoneNumber = c.String(),
        //                PhoneNumberConfirmed = c.Boolean(nullable: false),
        //                TwoFactorEnabled = c.Boolean(nullable: false),
        //                LockoutEndDateUtc = c.DateTime(),
        //                LockoutEnabled = c.Boolean(nullable: false),
        //                AccessFailedCount = c.Int(nullable: false),
        //                UserName = c.String(nullable: false, maxLength: 256),
        //            })
        //        .PrimaryKey(t => t.Id)
        //        .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
        //    CreateTable(
        //        "dbo.AspNetUserClaims",
        //        c => new
        //            {
        //                Id = c.Int(nullable: false, identity: true),
        //                UserId = c.String(nullable: false, maxLength: 128),
        //                ClaimType = c.String(),
        //                ClaimValue = c.String(),
        //            })
        //        .PrimaryKey(t => t.Id)
        //        .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
        //        .Index(t => t.UserId);
            
        //    CreateTable(
        //        "dbo.AspNetUserLogins",
        //        c => new
        //            {
        //                LoginProvider = c.String(nullable: false, maxLength: 128),
        //                ProviderKey = c.String(nullable: false, maxLength: 128),
        //                UserId = c.String(nullable: false, maxLength: 128),
        //            })
        //        .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
        //        .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
        //        .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.POIs", "LocalId", "dbo.Locals");
            DropForeignKey("dbo.Metereologias", "LocalId", "dbo.Locals");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.POIs", new[] { "LocalId" });
            DropIndex("dbo.Metereologias", new[] { "LocalId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.POIs");
            DropTable("dbo.Locals");
            DropTable("dbo.Metereologias");
        }
    }
}
