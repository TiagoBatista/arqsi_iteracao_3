namespace Cancela.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class models : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Metereologias", "hora_de_leitura", c => c.Time(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Metereologias", "hora_de_leitura", c => c.DateTime(nullable: false));
        }
    }
}
