namespace Cancela.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Sensor1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Facetas",
                c => new
                    {
                        FacetasID = c.Int(nullable: false, identity: true),
                        CampoBD = c.String(),
                        Nome = c.String(),
                        Grandeza = c.String(),
                        Tipo = c.String(),
                        Semantica = c.String(),
                        sensorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FacetasID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Facetas");
        }
    }
}
