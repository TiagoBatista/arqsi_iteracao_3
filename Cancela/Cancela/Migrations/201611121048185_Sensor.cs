namespace Cancela.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Sensor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sensors",
                c => new
                    {
                        SensorId = c.Int(nullable: false, identity: true),
                        nome = c.String(),
                        descricao = c.String(),
                    })
                .PrimaryKey(t => t.SensorId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Sensors");
        }
    }
}
