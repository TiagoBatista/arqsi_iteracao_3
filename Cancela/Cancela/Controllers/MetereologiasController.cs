﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Cancela.Models;
using ModelLibrary;
using System.Web.Http.Cors;
using ModelLibrary.IdentityModels;

namespace Cancela.Controllers
{
    [EnableCors(origins: "http://localhost:58965,http://localhost:65106", headers: "*", methods: "*")]
    public class MetereologiasController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Metereologias
        [Authorize(Roles ="Editor")]
        public IQueryable<Metereologia> GetMetereologias()
        {
            return db.Metereologias.Include(p=>p.local);
        }

        // GET: api/Metereologias/5
        [Authorize(Roles ="Editor")]
        [ResponseType(typeof(Metereologia))]
        public async Task<IHttpActionResult> GetMetereologia(int id)
        {
            Metereologia metereologia = await db.Metereologias.FindAsync(id);
            if (metereologia == null)
            {
                return NotFound();
            }

            return Ok(metereologia);
        }

        // PUT: api/Metereologias/5
        [Authorize(Roles ="Editor")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMetereologia(int id, Metereologia metereologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != metereologia.MetereologiaId)
            {
                return BadRequest();
            }

            db.Entry(metereologia).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MetereologiaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Metereologias
        [Authorize(Roles ="Editor")]
        [ResponseType(typeof(Metereologia))]
        public async Task<IHttpActionResult> PostMetereologia(Metereologia metereologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Metereologias.Add(metereologia);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = metereologia.MetereologiaId }, metereologia);
        }

        // DELETE: api/Metereologias/5
        [Authorize(Roles ="Editor")]
        [ResponseType(typeof(Metereologia))]
        public async Task<IHttpActionResult> DeleteMetereologia(int id)
        {
            Metereologia metereologia = await db.Metereologias.FindAsync(id);
            if (metereologia == null)
            {
                return NotFound();
            }

            db.Metereologias.Remove(metereologia);
            await db.SaveChangesAsync();

            return Ok(metereologia);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MetereologiaExists(int id)
        {
            return db.Metereologias.Count(e => e.MetereologiaId == id) > 0;
        }
    }
}