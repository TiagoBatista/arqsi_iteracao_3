﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Cancela.Models;
using System.Web.Http.Cors;
using ModelLibrary.Sensors;
using ModelLibrary.IdentityModels;

namespace Cancela.Controllers
{
    //[EnableCors(origins: "http://localhost:58965,http://localhost:65106", headers: "*", methods: "*")]
    public class FacetasController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Facetas 
        [Authorize(Roles = "Editor")]
        public List<Facetas> GetFacetas(int sensorid)
        {
            List<Facetas> facetas = new List<Facetas>();
            foreach (Facetas f in db.Facetas)
            {
                if (sensorid == f.sensorId)
                {
                    facetas.Add(f);
                }
            }
            return  facetas;

        }

        // GET: api/Facetas/5
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(Facetas))]
        public async Task<IHttpActionResult> GetFacetas(int id,int sensorid)
        {
            Facetas facetas=null;
            foreach (Facetas f in db.Facetas)
            {
                if (id==f.FacetasID & sensorid==f.sensorId)
                {
                    facetas = f;
                }
            }
            
            if (facetas == null)
            {
                return NotFound();
            }

            return Ok(facetas);
        }

        // PUT: api/Facetas/5
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutFacetas(int id, Facetas facetas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != facetas.FacetasID)
            {
                return BadRequest();
            }

            db.Entry(facetas).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FacetasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Facetas
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(Facetas))]
        public async Task<IHttpActionResult> PostFacetas(Facetas facetas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Facetas.Add(facetas);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = facetas.FacetasID }, facetas);
        }

        // DELETE: api/Facetas/5
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(Facetas))]
        public async Task<IHttpActionResult> DeleteFacetas(int id)
        {
            Facetas facetas = await db.Facetas.FindAsync(id);
            if (facetas == null)
            {
                return NotFound();
            }

            db.Facetas.Remove(facetas);
            await db.SaveChangesAsync();

            return Ok(facetas);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FacetasExists(int id)
        {
            return db.Facetas.Count(e => e.FacetasID == id) > 0;
        }
    }
}