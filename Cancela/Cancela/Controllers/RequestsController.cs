﻿using Cancela.Models;
using ModelLibrary;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ModelLibrary.IdentityModels;

namespace Cancela.Controllers
{
    public class RequestsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private MetereologiasController mc = new MetereologiasController();

        [ResponseType(typeof(List<Metereologia>))]
        public async Task<IHttpActionResult> GetMetereologias()
        {
            List<Metereologia> mete = mc.GetMetereologias().ToList();

            var r = HttpContext.Current.Request.QueryString;

            if (r!=null)
            {
                mete = GetMetereologiasResults(r);
            }

            return Ok(mete);
        }

        //[ResponseType(typeof(List<Metereologia>))]
        //public async Task<IHttpActionResult> GetMetereologiaByPOI(int id)
        //{
        //    POI pOI = await db.POIs.FindAsync(id);
        //    if (pOI == null)
        //    {
        //        return NotFound();
        //    }
        //    List<Metereologia> mete = mc.GetMetereologias().Where(x => x.LocalId == pOI.LocalId).ToList();

        //    return Ok(mete);
        //}

        [ResponseType(typeof(List<Metereologia>))]
        public async Task<IHttpActionResult> GetMetereologiaByDateAndHour(DateTime dt,TimeSpan ts)
        {
            List<Metereologia> mete = mc.GetMetereologias()
                .Where(x => x.Data_de_leitura == dt && x.Hora_de_leitura==ts)
                .ToList();
            

            return Ok(mete);
        }

        [ResponseType(typeof(List<Metereologia>))]
        public async Task<IHttpActionResult> GetMetereologiaByDateAndHourRange(DateTime dt1,DateTime dt2, TimeSpan ts1,TimeSpan ts2)
        {
            List<Metereologia> mete = mc.GetMetereologias()
                .Where(x => x.Data_de_leitura > dt1 && x.Data_de_leitura < dt2
                    && x.Hora_de_leitura > ts1 && x.Hora_de_leitura < ts2)
                .ToList();

            return Ok(mete);
        }


        
        private List<Metereologia> joinLists(List<Metereologia> first, List<Metereologia> second)
        {
            List<Metereologia> joined = new List<Metereologia>();
            if (!second.Any())
            {
                return first;
            }
            else
            {
                foreach (Metereologia m in first)
                {
                    if (second.Contains(m))
                    {
                        joined.Add(m);
                    }
                }
            }
            return joined;
        }
        
        public List<float> getValues(String DB)
        {
            List<float> l = new List<float>();
            switch (DB)
            {
                case "Temp":
                    l=db.Metereologias.Select(x => x.Temp).Distinct().ToList();
                    break;
                case "Humidade":
                    l=db.Metereologias.Select(x => x.Humidade).Distinct().ToList();
                    break;
                case "Vento":
                    l=db.Metereologias.Select(x => x.Vento).Distinct().ToList();
                    break;
                case "NO":
                    l=db.Metereologias.Select(x => x.NO).Distinct().ToList();
                    break;
                case "NO2":
                    l=db.Metereologias.Select(x => x.NO2).Distinct().ToList();
                    break;
                case "CO2":
                    l= db.Metereologias.Select(x => x.CO2).Distinct().ToList();
                    break;
                case "Pressao":             
                    l=db.Metereologias.Select(x => x.Pressao).Distinct().ToList();
                    break;
            }
            return l;
        }
        
        private List<Metereologia> GetMetereologiasResults(NameValueCollection qString)
        {
            List<Metereologia> list = mc.GetMetereologias().ToList();
            //check dates
            if (qString["Data_de_leitura1"] != null && qString["Data_de_leitura2"] != null)
            {
                DateTime t1 = DateTime.Parse(qString["Data_de_leitura1"]);
                DateTime t2 = DateTime.Parse(qString["Data_de_leitura2"]);

                List<Metereologia> second = mc.GetMetereologias()
                .Where(x => x.Data_de_leitura >= t1 && x.Data_de_leitura <= t2).ToList();
                list=joinLists(list, second);
            }

            //check hours
            if (qString["Hora_de_leitura1"] != null && qString["Hora_de_leitura2"] != null)
            {
                
                TimeSpan t1 = TimeSpan.Parse(qString["Hora_de_leitura1"]);
                TimeSpan t2 = TimeSpan.Parse(qString["Hora_de_leitura2"]);
                //horas selecionadas
                List<Metereologia> second = mc.GetMetereologias()
                .Where(x => x.Hora_de_leitura > t1 && x.Hora_de_leitura <= t2).ToList();
                list=joinLists(list, second);
            }

            //check Temp
            if (qString["Temp"] != null)
            {
                float temp = float.Parse(qString["Temp"]);
                List<Metereologia> second = mc.GetMetereologias().Where(x => x.Temp == temp).ToList();
                list=joinLists(list, second);
            }

            //check Vento
            if (qString["Vento"] != null)
            {
                float vento = float.Parse(qString["Vento"]);
                List<Metereologia> second = mc.GetMetereologias().Where(x => x.Vento == vento).ToList();
                list=joinLists(list, second);
            }

            //check Humidade
            if (qString["Humidade"]!=null)
            {
                float humidade = float.Parse(qString["Humidade"]);
                List<Metereologia> second = mc.GetMetereologias().Where(x => x.Humidade == humidade).ToList();
                list=joinLists(list, second);
            }

            //check Pressao
            if (qString["Pressao"] !=null)
            {
                float pressao = float.Parse(qString["Pressao"]);
                List<Metereologia> second = mc.GetMetereologias().Where(x => x.Pressao == pressao).ToList();
                list=joinLists(list, second);
            }

            //Check NO
            if (qString["NO"] != null)
            {
                float no = float.Parse(qString["NO"]);
                List<Metereologia> second = mc.GetMetereologias().Where(x => x.NO == no).ToList();
                list=joinLists(list, second);
            }

            //check NO2
            if (qString["NO2"] != null)
            {
                float no2 = float.Parse(qString["NO2"]);
                List<Metereologia> second = mc.GetMetereologias().Where(x => x.NO2 ==no2).ToList();
                list=joinLists(list, second);
            }

            //check CO2
            if (qString["CO2"]!=null)
            {
                float co2 = float.Parse(qString["CO2"]);
                List<Metereologia> second = mc.GetMetereologias().Where(x => x.CO2 ==co2).ToList();
                list=joinLists(list, second);
            }

            if (qString["POI"]!=null)
            {
                POI pOI = db.POIs.Find(int.Parse(qString["POI"]));
                if (pOI != null)
                {
                    List<Metereologia> mete = mc.GetMetereologias().Where(x => x.LocalId == pOI.LocalId).ToList();
                    list = joinLists(list, mete);
                }
                

            }

            return list;

        }

        //[ResponseType(typeof(List<Metereologia>))]
        //public async Task<IHttpActionResult> GetMetereologiaByPOIProximity(int id)
        //{
        //    POI pOI = await db.POIs.FindAsync(id);
        //    if (pOI == null)
        //    {
        //        return NotFound();
        //    }

        //    List<Metereologia> mete = db.Metereologias
        //        .Where(x => x.data_de_leitura > dt1 && x.data_de_leitura < dt2
        //            && x.hora_de_leitura > ts1 && x.hora_de_leitura < ts2)
        //        .ToList();


        //    return Ok(mete);
        //}

    }
}
